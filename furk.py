#!/usr/bin/python
# -*- coding: utf-8 -*-

from xml.etree import ElementTree as ET
import libxml2
import os
import sys

reload(sys)
sys.setdefaultencoding('utf8')

ns = "{http://xspf.org/ns/0/}"
def getlist(file):

	tree = ET.ElementTree()
	
	# parse xspf tree
	tree.parse(file)

	root = tree.getroot()

	if root.tag == ("%splaylist" % (ns)):
		print "yes"

	# get children
	child = root.getchildren()

	for i in child:
		if i.tag == ("%stitle" % (ns)):
			title = i.text
		if i.tag == ("%strackList" % (ns)):
			tracklist = i

	# mkdir
	if not os.path.isdir(title):
		os.mkdir(title)

	# tracklist
	for track in tracklist:
		trackchild = track.getchildren()

		for i in trackchild:
			if i.tag == ("%salbum" % (ns)):
				album = i.text
			if i.tag == ("%stitle" % (ns)):
				tracktitle = i.text
			if i.tag == ("%slocation" % (ns)):
				location = i.text

		# Sometime you will get empty album as "None"
		# Replace "None" with "" string
		if album == None:
			album = ""

		# dump info for each item
		# print album
		# print tracktitle
		# print location
	
		if not os.path.isdir((title + '/' + album)):
			os.mkdir((title + '/' + album))

		# convert to utf-8
		# u_tracktitle = unicode(tracktitle, "utf-8")

		path = title + '/' + album
		outfile = path + tracktitle

		if not os.path.exists(outfile):
			cmd = "wget -c " + "\""+ location + "\"" + " -O " + \
				"\"" + outfile + "\""
			os.system(cmd)
			if os.path.getsize(outfile) == 324230:
				os.remove(outfile)
				print "Limit has been exceeded\n"
				exit() 
		elif os.path.getsize(outfile) == 324230:
			print "Limit has been exceeded\n"
			print "Fail:" + outfile
			os.remove(outfile)
			exit()
		else:
			print "Skip: " + outfile

if __name__ == "__main__":
	file = sys.argv[1]

	getlist(file)
